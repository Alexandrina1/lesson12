package lesson12;

public class Profesor {
    String name;
    String specialitate;
    int age;
    float height;

    Profesor()
    {
        specialitate = "Tehnologie informationala";
    }
    Profesor(String name,  int age)
    {
        this();
        System.out.println(name + ' ' + age +" de ani, " + " " + "specialitatea este " + " " + specialitate);
    }
    public static void main(String[] args)
    {
        Profesor pr1 = new Profesor("Scrob Andrei ", 23);
    }
}


