package lesson12;

public class Universitate {
    int age;
    String name;
    float mediaStudenti;

    Universitate(int age) {

        this.age= age;
    }

    Universitate(String name) {

        this.name = name;
    }

    Universitate(float mediaStudenti) {
        this.mediaStudenti = mediaStudenti;
    }

    public static void main(String[] args) {
        Universitate ob1 = new Universitate("UTM");
        System.out.println(ob1.name);

        Universitate ob2 = new Universitate(519);
        System.out.println(ob2.age);

        Universitate ob3 = new Universitate(7.8f);
        System.out.println(ob3.mediaStudenti);

    }

}
