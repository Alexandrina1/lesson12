package lesson12;

public class Home {
    public void getStrada(Object strada)
    {

        System.out.println( " str. Tekwille 1/9 "+ strada.toString() );
    }

    public void showStrada()
    {
        getStrada(this);
    }

    public static void  main(String[] arg){
        Home ob1 = new Home();
        ob1.showStrada();
    }
}
