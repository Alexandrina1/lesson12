package lesson12;

public class Student {
    String name;
    int age;
    float media;


    public void setData(String name, int age, float media){
        this.name = name;
        this.age = age;
        this.media = media;
    }
    public void showData()

    {
        System.out.println(" name = " + name);
        System.out.println(" age = " + age);
        System.out.println(" media = " + media);

    }

public void printStudent()
{
    this.setData("Burdujan P.", 18, 8.5f);
    this.showData();
}
    public static void main(String[] args){
        Student st1 = new Student();
        st1.printStudent();

    }

}
